import sys
import json


class i3status_conf_parser:
    """
    Class that parse the i3status config
    """

    def __init__(self, path):
        self.conf_dict = {}
        self.conf_dict["order"] = []
        self.module = ""
        self.path = path

    def is_module_header(self, line):
        """
        Identify that is a module header
        in the config, write is name in the dict
        and set up module attribut that allow us
        to now that the parser is in a module.
        """
        if line.strip().endswith("{") and self.module == "":
            self.module = line.replace(" ", "").replace("{", "").replace("\n", "")
            self.conf_dict[self.module] = {}

    def is_module_argument(self, line):
        """
        Identify that is an argument of the module
        in config file and write it in the dict.
        """
        if "=" in line and self.module != "":
            key = line.split("=")[0].replace("+", "").strip()
            value = line.split("=")[1].strip()
            self.conf_dict[self.module][key] = value

    def is_module_footer(self, line):
        """
        Identify that is the end of a modulein the
        config file and reset the module attribut.
        """
        if line.strip().startswith("}") and self.module != "":
            self.module = ""

    def is_order(self, line):
        """
        Identify that the parser is not in a module,
        it get the order value and add it in a list
        created for it in the dict.
        """
        if line.strip().replace("\n", "").startswith("order") and self.module == "":
            self.conf_dict["order"].append(line.split("+=")[1].strip())

    def parse(self):
        """
        Open the config file,
        parse it line by line,
        and construct a dict.
        """
        conf_file = open(self.path, "r")
        for line in conf_file:
            self.is_module_header(line)
            self.is_module_argument(line)
            self.is_module_footer(line)
            self.is_order(line)
        conf_file.close()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        parser = i3status_conf_parser(sys.argv[1])
        parser.parse()
        print(
            "\n\n###################  FILE CONFIGURATION IN A DICT  ###################\n\n"
        )
        print(parser.conf_dict)
        print(
            "\n\n###################  FILE CONFIGURATION IN A JSON  ###################\n\n"
        )
        print(json.dumps(parser.conf_dict))
    else:
        print("Please specify a path for a configuration file in argument.")
