from py3status.parsers.config import parse_config
from py3status.types.config import ModuleConfig, RootConfig, SectionConfig

import os

_CURR_PATH = os.path.dirname(os.path.abspath(__file__))

_CONFIG_FILES_PATH = _CURR_PATH + "/config_files"
_CONFIG_FILES = {
    fn: f"{_CONFIG_FILES_PATH}/{fn}" for fn in os.listdir(_CONFIG_FILES_PATH)
}


def test_ok_i3status_default():
    config = parse_config(_CONFIG_FILES["ok_i3status_default"])
    expected_config = RootConfig(
        name="root",
        raw=open(_CONFIG_FILES["ok_i3status_default"]).read(),
        order=[
            "ipv6",
            "disk /",
            "wireless _first_",
            "ethernet _first_",
            "battery all",
            "load",
            "tztime local",
        ],
        childs=[
            SectionConfig(
                name="general",
                parent="root",
                properties={"colors": True, "interval": 5},
            ),
            ModuleConfig(
                name="wireless _first_",
                module="wireless",
                instance="_first_",
                parent="root",
                properties={
                    "format_up": "W: (%quality at %essid) %ip",
                    "format_down": "W: down",
                },
            ),
            ModuleConfig(
                name="ethernet _first_",
                module="ethernet",
                instance="_first_",
                parent="root",
                properties={"format_up": "E: %ip (%speed)", "format_down": "E: down"},
            ),
            ModuleConfig(
                name="battery all",
                module="battery",
                instance="all",
                parent="root",
                properties={"format": "%status %percentage %remaining"},
            ),
            ModuleConfig(
                name="tztime local",
                module="tztime",
                instance="local",
                parent="root",
                properties={"format": "%Y-%m-%d %H:%M:%S"},
            ),
            ModuleConfig(
                name="load",
                module="load",
                instance="",
                parent="root",
                properties={"format": "%1min"},
            ),
            ModuleConfig(
                name="disk /",
                module="disk",
                instance="/",
                parent="root",
                properties={"format": "%avail"},
            ),
        ],
    )
    assert config == expected_config


def test_ok_group():
    config = parse_config(_CONFIG_FILES["ok_group"])
    expected_config = RootConfig(
        name="root",
        raw=open(_CONFIG_FILES["ok_group"]).read(),
        order=["group tz"],
        childs=[
            SectionConfig(
                name="general",
                parent="root",
                properties={"colors": True, "interval": 5},
            ),
            ModuleConfig(
                name="group tz",
                module="group",
                instance="tz",
                parent="root",
                properties={"cycle": 10, "format": "{output}"},
                childs=[
                    ModuleConfig(
                        name="tztime la",
                        module="tztime",
                        instance="la",
                        parent="group tz",
                        properties={
                            "format": "LA %H:%M",
                            "timezone": "America/Los_Angeles",
                        },
                    ),
                    ModuleConfig(
                        name="tztime ny",
                        module="tztime",
                        instance="ny",
                        parent="group tz",
                        properties={
                            "format": "NY %H:%M",
                            "timezone": "America/New_York",
                        },
                    ),
                    ModuleConfig(
                        name="tztime du",
                        module="tztime",
                        instance="du",
                        parent="group tz",
                        properties={"format": "DU %H:%M", "timezone": "Asia/Dubai"},
                    ),
                ],
            ),
        ],
    )
    assert config == expected_config


def test_ok_list():
    config = parse_config(_CONFIG_FILES["ok_list"])
    expected_config = RootConfig(
        name="root",
        raw=open(_CONFIG_FILES["ok_list"]).read(),
        order=["volume_status speakers", "do_not_disturb", "velib_metropole"],
        childs=[
            SectionConfig(
                name="general",
                parent="root",
                properties={"colors": True, "interval": 5},
            ),
            ModuleConfig(
                name="volume_status speakers",
                module="volume_status",
                instance="speakers",
                parent="root",
                properties={
                    "command": "pactl",
                    "format": "{percentage}%",
                    "format_muted": "{percentage}%",
                    "button_down": 5,
                    "button_mute": 1,
                    "button_up": 4,
                    "thresholds": [(0, "bad"), (5, "degraded"), (10, "good")],
                    "max_volume": 200,
                },
            ),
            ModuleConfig(
                name="do_not_disturb",
                module="do_not_disturb",
                instance="",
                parent="root",
                properties={
                    "format": "[\?color=state [\?if=state |]]",  # noqa
                    "thresholds": [(0, "good"), (1, "bad")],
                },
            ),
            ModuleConfig(
                name="velib_metropole",
                module="velib_metropole",
                instance="",
                parent="root",
                properties={"stations": [14036, 14005, 14103]},
            ),
        ],
    )
    assert config == expected_config


def test_ok_dict():
    config = parse_config(_CONFIG_FILES["ok_dict"])
    expected_config = RootConfig(
        name="root",
        raw=open(_CONFIG_FILES["ok_dict"]).read(),
        order=["net_rate"],
        childs=[
            SectionConfig(
                name="general",
                parent="root",
                properties={"colors": True, "interval": 5},
            ),
            ModuleConfig(
                name="net_rate",
                module="net_rate",
                instance="",
                parent="root",
                properties={
                    "format": "{interface}: {up} {down}",
                    "precision": 0,
                    "fake_option": {"toto": 1, "tata": "ok"},
                },
            ),
        ],
    )
    print()
    print(config.childs)
    print()
    print(expected_config.childs)
    assert config == expected_config
