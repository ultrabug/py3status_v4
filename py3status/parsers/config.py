import shlex

from py3status.types.config import ModuleConfig, RootConfig, SectionConfig


GLOBAL_SECTIONS = ["general", "py3status"]


def remove_quotes(value):
    """
    Remove any surrounding quotes from a value and unescape any contained
    quotes of that type.
    """
    # beware the empty string
    if not value:
        return value

    if value[0] == value[-1] == '"':
        return value[1:-1].replace('\\"', '"')
    if value[0] == value[-1] == "'":
        return value[1:-1].replace("\\'", "'")
    return value


def make_value(lex):
    """
    """
    value = lex.get_token()

    if not value:
        return

    if value[0] == "[":
        while "]" not in value:
            value += lex.get_token()

        # list of tuples
        if "(" in value and ")" in value:
            elements = value[1:-1].split(")")
            elements = [element.split("(")[1] for element in elements if "(" in element]
            tuples = []
            for element in elements:
                tup = tuple([get_value(v) for v in element.split(",")])
                tuples.append(tup)
            return tuples

        # list of mixed
        return [get_value(val) for val in value[1:-1].split(",")]

    if value[0] == "{":
        while "}" not in value:
            value += lex.get_token()
        if len(value.split("{")) == 2 and ":" in value:
            value_dict = {}
            for component in value[1:-1].split(","):
                k, v = component.split(":")
                value_dict[k] = get_value(v)
            return value_dict

    return get_value(value)


def get_value(value):
    """
    Converts to actual value, or remains as string.
    """
    if value and value[0] in ['"', "'"]:
        return remove_quotes(value)
    try:
        return int(value)
    except ValueError:
        pass
    try:
        return float(value)
    except ValueError:
        pass
    if value.lower() == "true":
        return True
    if value.lower() == "false":
        return False
    if value.lower() == "none":
        return None
    return value


def get_section_config(lex, name, parent):
    """
    """
    try:
        module, instance = name.split(" ")
    except ValueError:
        module = name
        instance = ""
    if name in GLOBAL_SECTIONS:
        config = SectionConfig(name=name, parent=parent)
    else:
        config = ModuleConfig(
            module=module, name=name, instance=instance, parent=parent
        )
    token = ""
    section = ""
    while token is not None:
        token = lex.get_token()
        if token == "":
            break
        elif token == "}":
            break
        elif token == "{":
            config.childs.append(get_section_config(lex=lex, name=section, parent=name))
            section = ""
        elif token == "=":
            config.properties[section] = make_value(lex)
            if "format" in section and isinstance(config.properties[section], list):
                config.properties[section] = f"[{config.properties[section][0]}]"
            section = ""
        else:
            if section:
                section = f"{section} {token}"
            else:
                section = token
    return config


def parse_config(path):
    """
    """
    name = "root"
    with open(path) as configuration:
        root_config = RootConfig(name=name, raw=configuration.read())
        configuration.seek(0)
        lex = shlex.shlex(configuration, posix=True)
        lex.wordchars += "-"
        token = ""
        section = ""

        while token is not None:
            token = lex.get_token()
            if token == "":
                break
            elif token == "}":
                print(f"root: unexpected end of section found")
                section = ""
                continue
            elif token == "order":
                assert lex.get_token() == "+"
                assert lex.get_token() == "="
                root_config.order.append(lex.get_token())
            elif token == "{":
                root_config.childs.append(
                    get_section_config(lex=lex, name=section, parent=name)
                )
                section = ""
            else:
                if section:
                    section = f"{section} {token}"
                else:
                    section = token
    return root_config


if __name__ == "__main__":
    # root_config = parse_config("/home/alexys/.config/py3status/config")
    root_config = parse_config(
        "/home/alexys/github/py4status/tests/parsers/config_files/ok_i3status_default"
    )
    print(root_config.sections)
    print(root_config.order)
    for child in root_config.childs:
        print("|")
        print("|-", child)
