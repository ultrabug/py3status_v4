from dataclasses import dataclass, field
from typing import List, Dict, Any


@dataclass
class Config:
    name: str


@dataclass
class SectionConfig(Config):
    parent: str = ""
    properties: Dict[str, Any] = field(default_factory=dict)


@dataclass
class ChildConfig(Config):
    childs: List[Config] = field(default_factory=list)

    @property
    def sections(self) -> List[str]:
        return [child.name for child in self.childs]


@dataclass
class RootConfig(ChildConfig):
    order: List[str] = field(default_factory=list)
    raw: str = ""


@dataclass
class ModuleConfig(ChildConfig, SectionConfig):
    module: str = ""
    instance: str = ""
