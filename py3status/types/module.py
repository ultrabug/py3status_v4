from dataclasses import dataclass

from py3status.types.config import ModuleConfig


@dataclass
class Module:
    parent: Module  # noqa
    config: ModuleConfig
