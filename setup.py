"""
py3status
"""

from setuptools import find_packages, setup
import os
import sys

module_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "py3status")
sys.path.insert(0, module_path)

sys.path.remove(module_path)


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="py3status",
    version="v4_alpha",
    author="Ultrabug",
    author_email="ultrabug@ultrabug.net",
    description="py3status v4 development only",
    long_description="",
    url="https://gitlab.com/ultrabug/py3status_v4",
    download_url="https://gitlab.com/ultrabug/py3status_v4/tags",
    license="BSD",
    platforms="any",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
